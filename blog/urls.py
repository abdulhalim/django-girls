from django.conf.urls import url
from . import views

app_name='blog'
urlpatterns = [
	#url(r'^$', views.home, name='home')
	url(r'^$', views.post_list, name='post_list'),
	url(r'^detail/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
	url(r'^new/$', views.new_post, name='new_post'),
	url(r'^(?P<pk>\d+)/edit/$', views.post_edit, name='post_edit')
]